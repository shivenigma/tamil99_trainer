<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Typing Trainer</title>
	<script type="text/javascript" src="script.js"></script>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
	<div id="suggestions">Suggestions will run here</div>
	<textarea id="text" autofocus="true"></textarea>
	<div id="keycontainer">
		<table class="keyboard" cellspacing="10">
		    <tr>
		        <td id="192">` <br></td>
		        <td id="49">1</td>
		        <td id="50">2</td>
		        <td id="51">3</td>
		        <td id="52">4 <br></td>
		        <td id="53">5</td>
		        <td id="54">6</td>
		        <td id="55">7 <br></td>
		        <td id="56">8</td>
		        <td id="57">9 <br></td>
		        <td id="48">0 <br></td>
		        <td id="173">- <br></td>
		        <td id="61">= <br></td>
		        <td class="fade" id="8">&lt;--</td>
		    </tr>
		    <tr>
		        <td class="fade" id="9">tab</td>
		        <td id="81">ஆ <br></td>
		        <td id="87">ஈ <br></td>
		        <td id="69">ஊ <br></td>
		        <td id="82">ஐ</td>
		        <td id="84">ஏ <br></td>
		        <td id="89">ள</td>
		        <td id="85">ற</td>
		        <td id="73">ன</td>
		        <td id="79">ட</td>
		        <td id="80">ண</td>
		        <td id="219">ச</td>
		        <td id="221">ஞ <br></td>
		        <!--<td id="">\</td>-->
		    </tr>
		    <tr>
		        <td class="fade" id="20">caps</td>
		        <td id="65">அ</td>
		        <td id="83">இ</td>
		        <td id="68">உ</td>
		        <td id="70" style="text-decoration:underline; background:#f0f0f0;">ஃ</td>
		        <td id="71">எ</td>
		        <td id="72">க</td>
		        <td id="74" style="text-decoration:underline;background:#f0f0f0;">ப<br></td>
		        <td id="75">ம</td>
		        <td id="76">த</td>
		        <td id="59">ந</td>
		        <td id="222">ய</td>
		        <td id="13" colspan="2" class="fade">enter</td>
		    </tr>
		    <tr>
		        <td id="16" colspan="2" class="fade">shift</td>
		        <td id="90">ஔ</td>
		        <td id="88">ஓ</td>
		        <td id="67">ஒ</td>
		        <td id="86">வ</td>
		        <td id="66">ங</td>
		        <td id="78">ல</td>
		        <td id="77">ர</td>
		        <td id="188">,</td>
		        <td id="190">.</td>
		        <td id="191">ழ</td>
		        <td id="16i" colspan="2" class="fade">shift</td>
		    </tr>
		    <tr>
		        <td id="17" colspan="2" class="fade">ctrl</td>
		        <td id="91" colspan="2" class="fade">winkey</td>
		        <td id="18" colspan="2" class="fade">alt</td>
		        <td id="32" colspan="4" class="fade">space</td>
		        <td id="18i" colspan="2" class="fade">alt</td>
		        <td id="17i" colspan="2" class="fade">ctrl</td>
		    </tr>
		</table>
	</div>
</body>
</html>