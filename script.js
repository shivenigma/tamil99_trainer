var element,
	suggestion,
	lasttype,
	flag;
window.onload = 
	function()
	{
		suggestion = document.getElementById('suggestions');
		element = document.getElementById('text');
	}
window.onkeydown =
	function (event)
	{	
		if(event.keyCode == 16 || event.keyCode == 17 || event.keyCode == 18)//for special keys like control, shift, alt
		{
			flag = event.keyCode;
			animateKey(event.keyCode+'i','grey');
		}
		animateKey(event.keyCode,'grey');
		if(event.keyCode == 8)
			{
				lasttype=undefined;
			}
		if(typeof(keyBind[event.keyCode]) != 'undefined') // to check if any letter key is pressed
		{
			var combined = flag+''+lasttype+''+event.keyCode;
			console.log('combined = ',combined);
			if(typeof(keyBind[combined]) != 'undefined') //to check if it is an double press letter like 'kaa in tamil'
			{
				val = element.value;
				val = val.slice(0, - 1)+''+keyBind[combined]; // for removing the previous letter
				element.value = val; 
			}
			else
			{
				element.value+=keyBind[flag+''+event.keyCode];
				if(flag!='')
				{
					lasttype = flag+''+event.keyCode;
				}
				else
				{
					lasttype = event.keyCode;
				}
			}
			event.preventDefault();
		}
	}
window.onkeyup =
	function(event)
	{
		if(event.keyCode == 16 || event.keyCode == 17 || event.keyCode == 18) //for ctrl,alt,shiftkeys
		{
			flag='';
			animateKey(event.keyCode+'i','');
		}
		animateKey(event.keyCode,'');
	}
function animateKey(elem,colour)
{
		document.getElementById(elem).style.background=colour;
}
var keyBind = new Array();
keyBind[81] = "ஆ";
keyBind[87] = "ஈ";
keyBind[69] = "ஊ";
keyBind[82] = "ஐ";
keyBind[84] = "ஏ";
keyBind[89] = "ள";
keyBind[85] = "ற";
keyBind[73] = "ன";
keyBind[79] = "ட";
keyBind[80] = "ண";
keyBind[219] = "ச"; // { key
keyBind[221] = "ஞ"; // } key
keyBind[65] = "அ";
keyBind[83] = "இ";
keyBind[68] = "உ";
keyBind[70] = "ஃ";
keyBind[71] = "எ";
keyBind[72] = "க";
keyBind[74] = "ப";
keyBind[75] = "ம";
keyBind[76] = "த";
keyBind[59] = "ந"; // ; key
keyBind[222] = "ய"; // ' key
keyBind[90] = "ஔ";
keyBind[88] = "ஓ";
keyBind[67] = "ஒ";
keyBind[86] = "வ";
keyBind[66] = "ங";
keyBind[78] = "ல";
keyBind[77] = "ர";
keyBind[191] = "ழ"; // ? key
// single letters end here
//meyyeluthukal
keyBind[7270] = "க்";
keyBind[6670] = "ங்";
keyBind[21970] = "ச்";
keyBind[22170] = "ஞ்";
keyBind[7970] = "ட்";
keyBind[8070] = "ண்";
keyBind[7670] = "த்";
keyBind[5970] = "ந்";
keyBind[7470] = "ப்";
keyBind[7570] = "ம்";
keyBind[22270] = "ய்";
keyBind[7770] = "ர்";
keyBind[7870] = "ல்";
keyBind[8670] = "வ்";
keyBind[19170] = "ழ்";
keyBind[8970] = "ள்";
keyBind[8570] = "ற்";
keyBind[7370] = "ன்";
keyBind[168170] = "ஸ்"
keyBind[168770] = "ஷ்";
keyBind[166970] = "ஜ்";
keyBind[168270] = "ஹ்";
//ஆ
keyBind[7281] = "கா";
keyBind[6681] = "ஙா";
keyBind[21981] = "சா";
keyBind[22181] = "ஞா";
keyBind[7981] = "டா";
keyBind[8081] = "ணா";
keyBind[7681] = "தா";
keyBind[5981] = "நா";
keyBind[7481] = "பா";
keyBind[7581] = "மா";
keyBind[22281] = "யா";
keyBind[7781] = "ரா";
keyBind[7881] = "லா";
keyBind[8681] = "வா";
keyBind[19181] = "ழா";
keyBind[8981] = "ளா";
keyBind[8581] = "றா";
keyBind[7381] = "னா";
keyBind[168181] = "ஸா"
keyBind[168781] = "ஷா";
keyBind[166981] = "ஜா";
keyBind[168281] = "ஹா";
keyBind[168481] = "க்ஷா";
//இ
keyBind[7283] = "கி";
keyBind[6683] = "ஙி";
keyBind[21983] = "சி";
keyBind[22183] = "ஞி";
keyBind[7983] = "டி";
keyBind[8083] = "ணி";
keyBind[7683] = "தி";
keyBind[5983] = "நி";
keyBind[7483] = "பி";
keyBind[7583] = "மி";
keyBind[22283] = "யி";
keyBind[7783] = "ரி";
keyBind[7883] = "லி";
keyBind[8683] = "வி";
keyBind[19183] = "ழி";
keyBind[8983] = "ளி";
keyBind[8583] = "றி";
keyBind[7383] = "னி";
keyBind[168183] = "ஸி"
keyBind[168783] = "ஷி";
keyBind[166983] = "ஜி";
keyBind[168283] = "ஹி";
keyBind[168483] = "க்ஷி";
//ஈ
keyBind[7287] = "கீ";
keyBind[6687] = "ஙீ";
keyBind[21987] = "சீ";
keyBind[22187] = "ஞீ";
keyBind[7987] = "டீ";
keyBind[8087] = "ணீ";
keyBind[7687] = "தீ";
keyBind[5987] = "நீ";
keyBind[7487] = "பீ";
keyBind[7587] = "மீ";
keyBind[22287] = "யீ";
keyBind[7787] = "ரீ";
keyBind[7887] = "லீ";
keyBind[8687] = "வீ";
keyBind[19187] = "ழீ";
keyBind[8987] = "ளீ";
keyBind[8587] = "றீ";
keyBind[7387] = "னீ";
keyBind[168187] = "ஸீ"
keyBind[168787] = "ஷீ";
keyBind[166987] = "ஜீ";
keyBind[168287] = "ஹீ";
keyBind[168487] = "க்ஷீ";
//உ
keyBind[7268] = "கு";
keyBind[6668] = "ஙு";
keyBind[21968] = "சு";
keyBind[22168] = "ஞு";
keyBind[7968] = "டு";
keyBind[8068] = "ணு";
keyBind[7668] = "து";
keyBind[5968] = "நு";
keyBind[7468] = "பு";
keyBind[7568] = "மு";
keyBind[22268] = "யு";
keyBind[7768] = "ரு";
keyBind[7868] = "லு";
keyBind[8668] = "வு";
keyBind[19168] = "ழு";
keyBind[8968] = "ளு";
keyBind[8568] = "று";
keyBind[7368] = "னு";
keyBind[168168] = "ஸு"
keyBind[168768] = "ஷு";
keyBind[166968] = "ஜு";
keyBind[168268] = "ஹு";
keyBind[168468] = "க்ஷு";
//ஊ
keyBind[7269] = "கூ";
keyBind[6669] = "ஙூ";
keyBind[21969] = "சூ";
keyBind[22169] = "ஞூ";
keyBind[7969] = "டூ";
keyBind[8069] = "ணூ";
keyBind[7669] = "தூ";
keyBind[5969] = "நூ";
keyBind[7469] = "பூ";
keyBind[7569] = "மூ";
keyBind[22269] = "யூ";
keyBind[7769] = "ரூ";
keyBind[7869] = "லூ";
keyBind[8669] = "வூ";
keyBind[19169] = "ழூ";
keyBind[8969] = "ளூ";
keyBind[8569] = "றூ";
keyBind[7369] = "னூ";
keyBind[168169] = "ஸூ"
keyBind[168769] = "ஷூ";
keyBind[166969] = "ஜூ";
keyBind[168269] = "ஹூ";
keyBind[168469] = "க்ஷூ";
//எ
keyBind[7271] = "கெ";
keyBind[6671] = "ஙெ";
keyBind[21971] = "செ";
keyBind[22171] = "ஞெ";
keyBind[7971] = "டெ";
keyBind[8071] = "ணெ";
keyBind[7671] = "தெ";
keyBind[5971] = "நெ";
keyBind[7471] = "பெ";
keyBind[7571] = "மெ";
keyBind[22271] = "யெ";
keyBind[7771] = "ரெ";
keyBind[7871] = "லெ";
keyBind[8671] = "வெ";
keyBind[19171] = "ழெ";
keyBind[8971] = "ளெ";
keyBind[8571] = "றெ";
keyBind[7371] = "னெ";
keyBind[168171] = "ஸெ"
keyBind[168771] = "ஷெ";
keyBind[166971] = "ஜெ";
keyBind[168271] = "ஹெ";
keyBind[168471] = "க்ஷெ";
//ஏ
keyBind[7284] = "கே";
keyBind[6684] = "ஙே";
keyBind[21984] = "சே";
keyBind[22184] = "ஞே";
keyBind[7984] = "டே";
keyBind[8084] = "ணே";
keyBind[7684] = "தே";
keyBind[5984] = "நே";
keyBind[7484] = "பே";
keyBind[7584] = "மே";
keyBind[22284] = "யே";
keyBind[7784] = "ரே";
keyBind[7884] = "லே";
keyBind[8684] = "வே";
keyBind[19184] = "ழே";
keyBind[8984] = "ளே";
keyBind[8584] = "றே";
keyBind[7384] = "னே";
keyBind[168184] = "ஸே"
keyBind[168784] = "ஷே";
keyBind[166984] = "ஜே";
keyBind[168284] = "ஹே";
keyBind[168484] = "க்ஷே";
//ஐ
keyBind[7282] = "கை";
keyBind[6682] = "ஙை";
keyBind[21982] = "சை";
keyBind[22182] = "ஞை";
keyBind[7982] = "டை";
keyBind[8082] = "ணை";
keyBind[7682] = "தை";
keyBind[5982] = "நை";
keyBind[7482] = "பை";
keyBind[7582] = "மை";
keyBind[22282] = "யை";
keyBind[7782] = "ரை";
keyBind[7882] = "லை";
keyBind[8682] = "வை";
keyBind[19182] = "ழை";
keyBind[8982] = "ளை";
keyBind[8582] = "றை";
keyBind[7382] = "னை";
keyBind[168182] = "ஸை"
keyBind[168782] = "ஷை";
keyBind[166982] = "ஜை";
keyBind[168282] = "ஹை";
keyBind[168482] = "க்ஷை";
//ஒ
keyBind[7267] = "கொ";
keyBind[6667] = "ஙொ";
keyBind[21967] = "சொ";
keyBind[22167] = "ஞொ";
keyBind[7967] = "டொ";
keyBind[8067] = "ணொ";
keyBind[7667] = "தொ";
keyBind[5967] = "நொ";
keyBind[7467] = "பொ";
keyBind[7567] = "மொ";
keyBind[22267] = "யொ";
keyBind[7767] = "ரொ";
keyBind[7867] = "லொ";
keyBind[8667] = "வொ";
keyBind[19167] = "ழொ";
keyBind[8967] = "ளொ";
keyBind[8567] = "றொ";
keyBind[7367] = "னொ";
keyBind[168167] = "ஸொ"
keyBind[168767] = "ஷொ";
keyBind[166967] = "ஜொ";
keyBind[168267] = "ஹொ";
keyBind[168467] = "க்ஷொ";
//ஓ
keyBind[7288] = "கோ";
keyBind[6688] = "ஙோ";
keyBind[21988] = "சோ";
keyBind[22188] = "ஞோ";
keyBind[7988] = "டோ";
keyBind[8088] = "ணோ";
keyBind[7688] = "தோ";
keyBind[5988] = "நோ";
keyBind[7488] = "போ";
keyBind[7588] = "மோ";
keyBind[22288] = "யோ";
keyBind[7788] = "ரோ";
keyBind[7888] = "லோ";
keyBind[8688] = "வோ";
keyBind[19188] = "ழோ";
keyBind[8988] = "ளோ";
keyBind[8588] = "றோ";
keyBind[7388] = "னோ";
keyBind[168188] = "ஸோ"
keyBind[168788] = "ஷோ";
keyBind[166988] = "ஜோ";
keyBind[168288] = "ஹோ";
keyBind[168488] = "க்ஷோ";
//ஔ
keyBind[7290] = "கௌ";
keyBind[6690] = "ஙௌ";
keyBind[21990] = "சௌ";
keyBind[22190] = "ஞௌ";
keyBind[7990] = "டௌ";
keyBind[8090] = "ணௌ";
keyBind[7690] = "தௌ";
keyBind[5990] = "நௌ";
keyBind[7490] = "பௌ";
keyBind[7590] = "மௌ";
keyBind[22290] = "யௌ";
keyBind[7790] = "ரௌ";
keyBind[7890] = "லௌ";
keyBind[8690] = "வௌ";
keyBind[19190] = "ழௌ";
keyBind[8990] = "ளௌ";
keyBind[8590] = "றௌ";
keyBind[7390] = "னௌ";
keyBind[168190] = "ஸௌ"
keyBind[168790] = "ஷௌ";
keyBind[166990] = "ஜௌ";
keyBind[168290] = "ஹௌ";
keyBind[168490] = "க்ஷௌ";
//சிறப்பு எழுத்துக்கள்
keyBind[1681] = "ஸ";
keyBind[1687] = "ஷ";
keyBind[1669] = "ஜ";
keyBind[1682] = "ஹ";
keyBind[1684] = "க்ஷ";
keyBind[1689] = "ஸ்ரீ";
